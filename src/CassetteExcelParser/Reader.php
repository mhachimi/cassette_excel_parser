<?php namespace CassetteExcelParser;

Class Reader extends \PHPExcel_Reader_Excel2007{

	private $inputFile, $loadedFile, $worksheetsInfos;
	
	public function __construct($inputFile)
	{
		$this->inputFile = $inputFile;
	}
	
	public function getWorksheetNames()
	{
		return $this->listWorksheetNames($this->inputFile);
	}

	public function getLoadedFile()
	{
		$this->loadedFile = ($this->loadedFile) ? $this->loadedFile : $this->load($this->inputFile) ;	
		return $this->loadedFile;
	}

	public function worksheetsInfos()
	{
		$this->worksheetsInfos = ($this->worksheetsInfos) ? $this->worksheetsInfos : $this->listWorksheetInfo($this->inputFile);	
		return $this->worksheetsInfos;
	}

	public function getCellValue($sheet_id, $cell)
	{
		$excel_file = $this->getLoadedFile();
		$sheet = $excel_file->getSheet($sheet_id);
		return $sheet->getCell($cell)->getValue();
	}

	public function sheetTotalRows($sheetKey)
	{
		return $this->worksheetsInfos()[$sheetKey]['totalRows'];
	}

  public function securityScan($xml)
  {
      $xml = parent::securityScan($xml);
      return str_replace(['<s:', '</s:'], ['<', '</'], $xml);
  }
}
